
# README

This repository contains the "Benchmark model":

> Caiani, A., Godin, A., Caverzasi, E., Gallegati, M., Kinsella, S., & Stiglitz, J. E. (2016). Agent based-stock flow consistent macroeconomics: Towards a benchmark model. Journal of Economic Dynamics and Control, 69, 375-408.

# Prerequisites

- gradle
- JDK (tested with jdk8-openjdk)

No IDE is needed for the steps below. You can edit the source
files with any editor you like and run the build/run commands
below from the command line.

# Using this repository

Use 

```
git clone git@gitlab.com:zauster/benchmarkmodel.git
```

to create a local clone of this repository. Then we will use
`gradle` to download and build all dependencies for us:

```
gradle build
```

followed by

```
gradle run
```

to run the simulation.

The main configuration of this simulation is at
`model/modelBenchmark_light.xml`. There are also other
configurations in this folder.
